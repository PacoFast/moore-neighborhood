using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors
{
    class Program
    {
        static void Main(string[] args)
        {
            const int SIZE = 10;
            try
            {
                Console.Write("x = ");
                int x = int.Parse(Console.ReadLine()); 
                Console.Write("\ny = ");
                int y = int.Parse(Console.ReadLine());
                if (x < 0 || x >= SIZE || y < 0 || y >= SIZE)
                {
                    Console.Write("Datos incorrectos!");
                    Console.ReadKey();
                    return; 
                }
                CalculeNeighbors(x, y, SIZE);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
                return; 
            }
            Console.ReadKey();
        }

        static void CalculeNeighbors(int x, int y, int SIZE)
        {
            int[,,] Array3d = new int[3, 3, 2]
            {
                {{ -1,-1}, { -1,0}, { -1,1} },
                {{ 0,-1 }, { 0,0 }, { 0,1 } },
                {{ 1,-1 } , { 1,0 }, { 1,1} },
            };
            int xCell, yCell=0; 
            for (int i = 0; i < Array3d.GetLength(0); i++)
            {
                for (int j = 0; j < Array3d.GetLength(1); j++)
                {
                    for (int z = 0; z < Array3d.GetLength(2); z++)
                    {
                        xCell = x + Array3d[i, j, z];
                        if (z + 1 < Array3d.GetLength(2))
                            yCell = y + Array3d[i, j, (z + 1)];

                        if(isValidRange(xCell, yCell, SIZE))
                            Console.Write($"[{xCell}, {yCell}]");
                        z = Array3d.GetLength(2); 
                    }
                }
                Console.WriteLine("");

            }
        }
        static bool isValidRange(int x, int y, int SIZE){return ((x < 0 || x >= SIZE) || (y < 0 || y >= SIZE)) ? false : true;}
    }
}
